# martinivanov-net

> Vue/Vuex/Vuetify client for the WordPress API 
(https://wordpress.org/plugins/json-api/) created by 
Martin Ivanov (https://wemakesites.net, https://martinivanov.net)

## Requirements
- NPM, WebPack, Babel
- Web server with PHP support
- Installation of WordPress
- Installed JSON-API plugin (https://wordpress.org/plugins/json-api/)
- Pretty URLs should be enabled in WordPress (http://domain.com/wp-admin/options-permalink.php)
in the following format: http://domain.com/2017/12/05/sample-post/
- To generate a sitemap, check the /sitemap-generator page in the app (scroll down to XML Sitemap)

## Application Configuration

1. Add meta tags and <title /> to index.html
2. Modify /src/api-proxy.php with your own settings and upload onto your server
3. Make sure you have a Google Analytics key and add it in Step 3
4. Modify /src/data/lang.js and /src/data/config.json and recompile
5. Modify /static/manifest.json
6. Replace favicon.ico in /static/icons wth your own
7. To display the announcement bar, configure it in /static/data/async-config.json. 
   Empty string in announcement.text means that the bar will not be displayed. 
   Check the other configuration aspects at https://vuetifyjs.com/components/alerts
8. For the typography that you can use when writing posts in WordPress, check https://vuetifyjs.com/style/typography
9. To change the order of the items in the navigation drawer, modify /src/components/NavigationBarItems.vue and recompile

## Building the App

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, 
check out the [guide](http://vuejs-templates.github.io/webpack/) 
and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Deployment

Upload the contents of the /dist folder onto your server

## Contact Me
To get in touch with me, send a message via https://wemakesites.net/contact
