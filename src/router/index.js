import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/pages/HomePage'
import TagPage from '@/pages/TagPage'
import CategoryPage from '@/pages/CategoryPage'
import PostPage from '@/pages/PostPage'
import PagePage from '@/pages/PagePage'
// import AuthorPage from '@/pages/AuthorPage'
import NotFound from '@/pages/NotFound'
import SearchResults from '@/pages/SearchResults'
import SiteMapGenerator from '@/pages/SiteMapGenerator'
import VueAnalytics from 'vue-analytics'
import Config from '../data/config.js'

Vue.use(Router)

const routes = [{
  path: '/',
  name: 'HomePage',
  component: HomePage
}, {
  path: '/sitemap-generator',
  component: SiteMapGenerator
}, {
  path: '/author',
  redirect: '/404'
}, {
  path: '/search',
  redirect: '/404'
}, {
  path: '/search/:page',
  redirect: '/404'
}, {
  path: '/404',
  name: 'NotFound',
  component: NotFound
}, {
  path: '/author/:id',
  redirect: '/page/about'
}, {
  path: '/tag',
  name: 'TagPage',
  component: TagPage,
  children: [{
    path: ':id',
    component: TagPage
  }]
}, {
  path: '/category',
  name: 'CategoryPage',
  component: CategoryPage,
  children: [{
    path: ':id',
    component: CategoryPage
  }]
}, {
  path: '/:year/:month/:day/:id',
  name: 'PostPage',
  component: PostPage
}, {
  path: Config.pageTopSlug,
  redirect: '/404'
}, {
  path: `${Config.pageTopSlug}/:slug`,
  component: PagePage,
  name: 'PagePage'
}, {
  path: '/search/:page/:query',
  name: 'SearchResults',
  component: SearchResults
}]

const router = new Router({
  mode: 'history',
  routes,
  scrollBehavior () {
    return {
      x: 0,
      y: 0
    }
  }
})

Vue.use(VueAnalytics, {
  id: Config.googleAnalyticsKey,
  router
})

export default router
