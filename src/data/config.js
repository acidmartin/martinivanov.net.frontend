/**
 * Application main configuration file
 * This file controls many aspects of how the
 * app works and behaves
 * **/
const Config = {
  /**
   * Site domain
   * **/
  domain: 'https://martinivanov.net',
  /**
   * Path to api-proxy.php file
   * **/
  apiProxyUrl: 'https://api.martinivanov.net/api-proxy.php',
  /**
   * Google analytics key
   * **/
  googleAnalyticsKey: 'UA-17572030-16',
  /**
   * Google AdSense Client ID
   * **/
  googleAdSenseClientId: 'ca-pub-4259822914193810',
  /**
   * Blog author slug
   * **/
  author: 'acidmartin',
  /**
   * Site base color (check colors here: https://vuetifyjs.com/style/colors)
   * **/
  baseColor: 'grey darken-3 theme--dark',
  /**
   * Route for "page" types, like /page/page-type
   * **/
  pageTopSlug: '/page',
  /**
   * Contact page, used in the top bar link
   * You could use:
   * 1. Local page, like /contact (don't forget the leading slash)
   * 2. External page, like https://wemakesites.net/contact
   * 3. Leave blank (the button will not render)
   * **/
  contactPage: 'https://wemakesites.net/contact',
  /**
   * Slug for the page, which content is shown in the info box in the navigation bar
   * **/
  infoBoxPageSlug: 'about',
  /**
   * Number of the latest posts shown on the home page
   * **/
  homePagePostsCount: 12,
  /**
   * Set higher count in order to get all posts/pages, etc
   * **/
  unlimitedPostsLimit: 1000,
  /**
   * Do not show entries in the categories cloud if they contain equal
   * or less than categoriesCloudMinCount posts/pages
   * **/
  categoriesCloudMinCount: 5,
  /**
   * Do not show entries in the tag cloud if they contain equal
   * or less than tagCloudMinCount posts/pages
   * **/
  tagCloudMinCount: 5,
  /**
   * Cookie which is set when the "dismiss" button of the
   * cookie consent bar at the bottom has been clicked
   * **/
  consentCookieName: '_martinvanovnet_cookie_consent_read',
  /**
   * Contents of the home page
   * You can set:
   * 1. 'latest-posts' to display the latest posts
   * 2. 'redirect:page-slug' to redirect to a specific page
   * 3. 'page:page-slug' to show the contents of a specific page without redirecting
   * **/
  homePageContents: 'latest-posts',
  /**
   * Sidebar settings
   * **/
  sidebar: {
    position: 'left', // Position of the sidebar, 'left' or 'right'
    items: {
      searchBar: true, // Display the search bar
      infoBox: true, // Display the info box
      categoriesCloud: true, // Display the categories cloud
      tagCloud: true, // Display the tag cloud,
      latestPosts: true, // Display the latest blog posts
      customBoxes: [{
        heading: 'Vuecidity', // optional
        description: 'Vuecidity is a free, open source and licensed under MIT component library for the Vue.js framework, inspired by Google Material Design and Bootstrap', // optional
        imageHeight: 166, // optional height of the image in pixels, default is 200 pixels
        imageUrl: 'https://api.martinivanov.net/wp-content/uploads/2018/03/vuecidity.png', // optional
        linkUrl: 'http://vuecidity.wemakesites.net/', // optional
        render: true // Display the box or not
      }, {
        heading: 'VueRibbon', // optional
        description: 'VueRibbon is a JavaScript, CSS3 and HTML5 implementation of the Microsoft Office ribbon control, built on top of Vue, Vuetify and ES6', // optional
        imageHeight: 166, // optional height of the image in pixels, default is 200 pixels
        imageUrl: 'https://api.martinivanov.net/wp-content/uploads/2018/01/vue-ribbon.png', // optional
        linkUrl: 'https://vueribbon.com/', // optional
        render: true // Display the box or not
      }, {
        heading: 'Ribbon.JS', // optional
        description: 'Arguably the best recreation of the MS Officetm ribbon bar, using JavaScript, CSS3, and HTML5.', // optional
        imageHeight: 166, // optional height of the image in pixels, default is 200 pixels
        imageUrl: 'https://api.martinivanov.net/wp-content/uploads/2017/12/ribbon.js.png', // optional
        linkUrl: 'http://ribbonjs.com/', // optional
        render: true // Display the box or not
      }, {
        heading: 'icons.io',
        description: 'Search, preview and use thousands of icons from Google Material Design, Font Awesome and more.',
        imageHeight: 166,
        imageUrl: 'https://api.martinivanov.net/wp-content/uploads/2018/01/icons.io_.png',
        linkUrl: 'http://icons.wemakesites.net/',
        render: true
      }, {
        heading: 'Encyclopaedia Metallum API',
        description: 'Unofficial, third party RESTful API for Encyclopaedia Metallum.',
        imageHeight: 166,
        imageUrl: 'https://api.martinivanov.net/wp-content/uploads/2016/11/em-api-thumb.png',
        linkUrl: 'http://em.wemakesites.net/',
        render: true
      }, {
        heading: 'Avid Eleven Rack Presets',
        description: 'The new sharing and community platform for Avid Eleven Rack presets and rigs.',
        imageHeight: 166,
        imageUrl: 'https://api.martinivanov.net/wp-content/uploads/2017/12/elevenrack.net_.png',
        linkUrl: 'http://elevenrack.net/',
        render: true
      }, {
        heading: 'HTML5, CSS3 and JavaScript Experiments and Insight',
        description: 'The demo website for the HTML5, CSS3 and JavaScript articles I\'ve been writing since 2006 here on my web-development blog.',
        linkUrl: 'http://experiments.wemakesites.net/',
        render: true
      }, {
        heading: 'Online Image to Base-64 Converter',
        description: 'HTML5-driven image to base-64 encoder.',
        linkUrl: 'http://image2base64.wemakesites.net/',
        render: true
      }],
      /**
       * Internal pages to show in a menu in the sidebar
       * **/
      pages: {
        heading: 'Menu', // Menu heading
        links: [{
          title: 'About Me', // Link label
          slug: 'about' // Link slug (do add leading slash)
        }, {
          title: 'Cookie Policy',
          slug: 'cookie-policy-for-this-blog'
        }]
      }
    }
  }
}

export default Config
