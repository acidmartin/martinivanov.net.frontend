/**
 * Language file for reusable string,
 * available application-wide
 * **/
const Lang = {
  blogTitle: 'HTML5, CSS3 and JavaScript', // Main title of the blog
  blogTagline: 'The Fine Art of Web Development by Martin Ivanov', // blog headline
  previousPost: 'Previous Post', // Label for the "Previous Post" button
  nextPost: 'Next Post', // Label for the "Next Post" button
  readFullPost: 'Continue reading...', // Label for the "Continue reading" button
  resultsPerPage: 'Results per page', // Label for the "Results per page" drop down on the search page
  home: 'Home', // Tooltip for the "home" button on the main toolbar
  aboutMe: 'About me', // Tooltip for the "about" button on the main toolbar
  searchResultsForTitle: 'Search results for', // Search results for...
  dateFormat: 'MMMM D, YYYY', // Default date format for display
  categories: 'Categories', // Categories title
  categoryArchives: 'Category Archives', // Categories archive title
  tagArchives: 'Tag Archives', // Tag archives title
  tags: 'Tags', // Tags title
  contactMe: 'Get in touch', // Tooltip for the "contact" button on the main toolbar
  cookieConsentMessage: 'I use cookies to ensure you get the best experience', // Message displayed in the cookies consent bar
  sitemapGeneratorPageTitle: 'Sitemap Generator', // /sitemap-generator page slug,
  latestPosts: 'Recent Posts' // Latest posts headline
}

export default Lang
