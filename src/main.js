import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import Lang from './data/Lang'
import Config from './data/config.js'
import VueAdsense from './vendor/vue-adsense/index'
import '../node_modules/vuetify/dist/vuetify.min.css'
import './App.css'

Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueAdsense)

Vue.config.productionTip = false

/**
 * Vuex state store
 */
const store = new Vuex.Store({
  state: {
    pages: {},
    domain: Config.domain,
    apiProxyUrl: Config.apiProxyUrl,
    baseColor: Config.baseColor,
    bootstrapData: {
      recentPosts: [],
      tags: [],
      categories: [],
      infoBox: {}
    },
    events: {
      ajax: 'ajax',
      pageLoaded: 'page:loaded'
    },
    posts: {},
    lang: Lang,
    categories: {},
    tags: {},
    externalData: {},
    googleAdSenseClientId: Config.googleAdSenseClientId,
    consentCookieName: Config.consentCookieName,
    sidebar: Config.sidebar,
    pageTopSlug: Config.pageTopSlug,
    homePageContents: Config.homePageContents,
    author: Config.author,
    unlimitedPostsLimit: Config.unlimitedPostsLimit,
    contactPage: Config.contactPage,
    categoriesCloudMinCount: Config.categoriesCloudMinCount,
    tagCloudMinCount: Config.tagCloudMinCount,
    asyncConfig: '/static/data/async-config.json',
    adBlockPlusEnabled: false
  },
  mutations: {
    setBootstrapData (state, bootstrapData) {
      state.bootstrapData = bootstrapData
    },
    storePost (state, bootstrapData) {
      const slug = bootstrapData.post.slug
      state.posts[slug] = bootstrapData
    },
    setPage (state, pageData) {
      const slug = pageData.slug
      state.pages[slug] = pageData
    },
    setAdBlockPlusEnabled (state) {
      document.querySelector('html').style.overflow = 'hidden'
      document.querySelector('body').style.overflow = 'hidden'
      state.adBlockPlusEnabled = true
    },
    setCategory (state, categoryData) {
      state.categories[categoryData.slug] = categoryData.pages
    },
    setTag (state, tagData) {
      state.tags[tagData.slug] = tagData.pages
    },
    setExternalData (state, externalData) {
      state.externalData = externalData
    }
  },
  getters: {
    sidebar: state => {
      return state.sidebar
    },
    bootstrap: state => {
      return state.bootstrap
    },
    categories: state => {
      return state.bootstrapData.categories
    },
    adsense: state => {
      return state.googleAdSenseClientId
    },
    tags: state => {
      return state.bootstrapData.tags
    },
    apiProxyUrl: state => {
      return state.apiProxyUrl
    },
    externalData: state => {
      return state.externalData
    },
    post: (state) => (id) => {
      return state.posts[id]
    },
    getLangKey: (state) => (key) => {
      return state.lang[key]
    },
    category: (state) => (slug) => {
      return state.categories[slug]
    },
    tag: (state) => (slug) => {
      return state.tags[slug]
    },
    getInfoBox: state => {
      return state.bootstrapData.infoBox
    },
    page: (state) => (slug) => {
      return state.pages[slug]
    }
  }
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  template: '<App/>',
  components: {
    App
  },
  created () {
    this.bootstrap()
  },
  data () {
    return {
      asyncConfig: '/static/data/async-config.json', // data loaded after the app bootstrap with ajax
      homePagePostsCount: Config.homePagePostsCount, // number of post excerpts to show on the home page,
      infoBoxPageSlug: Config.infoBoxPageSlug // slug of the page, which content will be displayed in the info box in the sidebar
    }
  },
  computed: {
    /**
     * Get the bootstrap URLS
     */
    getBootstrapUrls () {
      return this.$store.getters.bootstrap
    },
    /**
     * Get the API endpoint
     */
    getProxyUrl () {
      return this.$store.getters.apiProxyUrl
    }
  },
  methods: {
    /**
     * Create an API url from a slug
     * @filter makeProxyUrl
     * @param {String} slug
     * @return {String}
     */
    makeProxyUrl (slug) {
      return `${this.getProxyUrl}${slug}`
    },
    /**
     * Bootstrap the initially required data and mount the app
     * @filter bootstrap
     * @return {void}
     */
    bootstrap () {
      const url = `${this.getProxyUrl}?bootstrap=true&count=${this.homePagePostsCount}&infoBarPageSlug=${this.infoBoxPageSlug}`
      const $http = this.$http
      let bootstrapData = {}
      $http(url).then(response => {
        response = response.data
        const infoBox = response.infoBox
        bootstrapData = {
          recentPosts: response.recentPosts.posts,
          tags: response.tags,
          categories: response.categories,
          infoBox
        }
        this.$store.commit('setBootstrapData', bootstrapData)
        this.$store.commit('setPage', infoBox)
        this.$http(this.asyncConfig).then(response => {
          this.$store.commit('setExternalData', response.data)
          this.$mount('#app')
        })
      })
    }
  }
})
