import VueAdsense from './vue-adsense'

VueAdsense.install = function install (Vue) {
  Vue.component(VueAdsense.name, VueAdsense)
}

export default VueAdsense
