/**
 * Mixins, used application-wide
 * **/
const Mixins = {
  methods: {
    /**
     * Create date WordPress style - /YYYY/MM/DD/
     * @method createPostUrl
     * @param {String} date
     * @return {String}
     * **/
    getWpDate (date) {
      return date.split(/\s/)[0].replace(/-/g, '/')
    },
    /**
     * Create post params for Vue Router WordPress style - /YYYY/MM/DD/
     * @method createPostUrl
     * @param {String} date
     * @return {Object}
     * **/
    createWpDateSlug (date) {
      date = this.getWpDate(date).split('/')
      return {
        year: date[0],
        month: date[1],
        day: date[2]
      }
    },
    /**
     * Create post slug WordPress style - /YYYY/MM/DD/
     * @method createPostSlug
     * @param {String} date
     * @param {String} slug
     * @return {String}
     * **/
    createPostSlug (date, slug) {
      const id = slug
      const dateSlug = this.createWpDateSlug(date)
      dateSlug.id = id
      return dateSlug
    },
    /**
     * Format a URL WordPress style - /YYYY/MM/DD/post-slug
     * @method createPostUrl
     * @param {String} date
     * @param {String} slug
     * **/
    createPostUrl (date, slug) {
      date = date.split(' ')[0].split('-').join('/')
      const url = `/${date}/${slug}`
      return url
    },
    /**
     * Remove the domain part from URL
     * @method removeDomainFromUrl
     * @param {String} url
     * @return {String}
     * **/
    removeDomainFromUrl (url) {
      return url.replace(/^.*\/\/[^/]+/, '').replace('/', '').replace(/\/$/, '')
    },
    /**
     * Set document.title
     * @method setTitle
     * @param {String} newPageTitle [optional]
     * **/
    setTitle (newPageTitle) {
      const store = this.$store
      const getters = store.getters
      let title
      let delimiter = '-'
      const blogTitle = getters.getLangKey('blogTitle')
      const blogTagline = getters.getLangKey('blogTagline')
      if (!newPageTitle) {
        title = `${blogTitle} ${delimiter} ${blogTagline}`
      } else {
        title = `${newPageTitle} ${delimiter} ${blogTitle}`
      }
      document.title = `${title}`
      this.$root.$emit(store.state.events.pageLoaded)
    },
    /**
     * Create Vue Router route from URL
     * @method createRouteFromUrl
     * @param {String} url
     * @return {Function}
     * **/
    createRouteFromUrl (url) {
      url = this.removeDomainFromUrl(url).split('/')
      const slug = url.pop()
      const date = url.splice(0, 3).join('/')
      return this.createPostSlug(date, slug)
    },
    /**
     * Go to a page (/:slug), 'PagePage' route
     * @method goToPage
     * @param {String}
     * @return void
     * **/
    goToPage (slug) {
      return this.$router.push({
        name: 'PagePage',
        params: {
          slug: slug
        }
      })
    },
    /**
     * Remove HTML tags from string
     * @method sanitize
     * @param {String} html
     * @return {String}
     * **/
    sanitize (html) {
      return html.replace(/<\/?[^>]+(>|$)/g, '')
    },
    /**
     * Cap html/text at specified point and add ellipsis symbol
     * @method cap
     * @param {String} html
     * @param {Number} cap [optional]
     * @param {String} ellipsis [optional]
     * @return {String}
     * **/
    cap (html, cap, ellipsis) {
      html = this.sanitize(html)
      let visibleCharacters = html.substring(0, cap || 100)
      let lastWord = visibleCharacters.split(' ').pop()
      visibleCharacters = visibleCharacters.replace(new RegExp(lastWord + '$'), '')
      return `${visibleCharacters} ${ellipsis || '...'}`
    },
    /**
     * Get cookie
     * @method setCookie
     * @param {String} name
     * @private
     * @return {String}
     * **/
    getCookie (name) {
      const dc = document.cookie
      const prefix = name + '='
      let begin = dc.indexOf('; ' + prefix)

      if (begin === -1) {
        begin = dc.indexOf(prefix)
        if (begin !== 0) {
          return null
        }
      } else {
        begin += 2
        var end = document.cookie.indexOf(';', begin)

        if (end === -1) {
          end = dc.length
        }
      }
      return unescape(dc.substring(begin + prefix.length, end))
    },
    /**
     * Set cookie
     * @method setCookie
     * @param {String} name
     * @param {String} value
     * @param {Number} days
     * @private
     * @return void
     * **/
    setCookie (name, value, days) {
      const d = new Date()
      let expires

      d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000))
      expires = `expires=${d.toUTCString()}`
      document.cookie = `${name}=${value};${expires}`
    },
    /**
     * 404 redirect
     * @method to404
     * @return void
     * **/
    to404 () {
      this.$router.push({
        path: '/404'
      })
      this.$root.$emit(this.$store.state.events.ajax, false)
    },
    /**
     * Apply opacity according to a count
     * @method to404
     * @param {Number} count
     * @return {Number}
     * **/
    setOpacity (count) {
      if (count <= 5) {
        return 0.2
      } else if (count <= 7) {
        return 0.4
      } else if (count <= 10) {
        return 0.5
      } else if (count <= 15) {
        return 0.6
      } else if (count <= 20) {
        return 0.7
      } else if (count <= 25) {
        return 0.8
      } else if (count <= 30) {
        return 0.9
      } else {
        return 1
      }
    }
  }
}

export default Mixins
