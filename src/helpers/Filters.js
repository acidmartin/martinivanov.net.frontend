import Vue from 'vue'
import moment from 'moment'
import Lang from '../data/Lang'

/**
 * Format a date string to human-readable string
 * @filter formatDate
 * @param {DateString} date
 * @return {String}
 */
Vue.filter('formatDate', function (date) {
  if (date) {
    return moment(String(date)).format(Lang.dateFormat)
  }
})

export default Vue.filter
