<?php
ob_start('ob_gzhandler');
date_default_timezone_set('Europe/Brussels');
$SITE_TITLE = 'HTML5, CSS3 and JavaScript';
$SITE_SUBTITLE = 'The Fine Art of Web Development by Martin Ivanov';
$TITLE_SEPARATOR = ' - ';
$PAGE_TITLE = $SITE_TITLE . $TITLE_SEPARATOR . $SITE_SUBTITLE;
function compress($buffer) {
    $search = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s', '/\> \</');
    $replace = array('>', '<', '\\1', '><');
    $buffer = preg_replace($search, $replace, $buffer);
    return $buffer;
}

$api_url = 'https://api.martinivanov.net';
$url = $_SERVER['HTTP_HOST'];
$uri = $_SERVER['REQUEST_URI'];
$json = '?json=1';
$request_url = $api_url . $uri;
$explode = explode('/', $request_url);
$type = '';

$page_content = '';
$page_title = '';
$page_keywords = '';
$page_description = '';
$categories_html = '';
$tags_html = '';
$tag_tag_html = '';
$category_category_html = '';
$recent_posts_html = '';
$api_call_url = $request_url . $json;
//echo count($explode);
if (count($explode) === 7) {
    $type = 'blogpost';
} elseif (count($explode) === 5) {
    if (strpos($request_url, 'pages')) {
        // page/:page
    } else if (strpos($request_url, 'tag')) {
        // tag/:tag
        $type = 'tag:tag';
        $json = '/tag/' . end($explode) . '/?json=1';
        $api_call_url = $api_url . $json;
    } else if (strpos($request_url, 'category')) {
        // category/:category
        $type = 'category:category';
        $json = '/category/' . end($explode) . '/?json=1';
        $api_call_url = $api_url . $json;
    }
} elseif (count($explode) === 4) {
    if (strpos(end($explode), 'category') !== false) {
        $type = 'category';
        $json = '/api/get_category_index';
        $api_call_url = $api_url . $json;
    } elseif (strpos(end($explode), 'tag') !== false) {
        $type = 'tag';
        $json = '/api/get_tag_index';
        $api_call_url = $api_url . $json;
    } elseif (end($explode) === '') {
        $type = 'home';
        $api_call_url = $api_url . '/api/get_recent_posts';
    }
}
//echo $api_call_url;
if (@file_get_contents($api_call_url)) {
    $data = file_get_contents($api_call_url);
}

switch ($type) {
    case 'home':
        $recent_posts = json_decode($data) -> posts;
        $page_title = 'Home';
        $page_description = $page_title;
        $html = '<ul>';
        if ($recent_posts) {
            foreach ($recent_posts as $post) {
                $url = str_replace($api_url, '', $post -> url);
                $url = rtrim($url, '/');
                $url = str_replace($api_url, '', $post -> url);
                $url = rtrim($url, '/');
                $html .= '<li>
              <h4><a href="' . $url . '">' . $post -> title_plain . '</a></h4>
              <div>' . strip_tags($post -> excerpt) . '</div>
            </li>';
            }
        }
        $html .= '</ul>';
        $recent_posts_html = $html;
        break;
    case 'category':
        $categories = json_decode($data) -> categories;
        $page_title = 'Categories';
        $page_description = $page_title;
        $html = array();
        $categories_html .= '<ul><li><a href="/category">categories home</a></li>';
        $keywords = array();
        if ($categories) {
            foreach ($categories as $category) {
                $keywords[] = $category -> title;
                $categories_html .= '<li><a href="/category/'. $category -> slug .'">' . $category -> title . '</a></li>';
            }
        }
        $categories_html .= '</ul>';
        $page_keywords = implode(',', $keywords);
        break;
    case 'tag':
        $tags = json_decode($data) -> tags;
        $html = array();
        $tags_html .= '<ul><li><a href="/tag">tags home</a></li>';
        $keywords = array();
        $page_title = 'Tags';
        $page_description = $page_title;
        if ($tags) {
            foreach ($tags as $tag) {
                $keywords[] = $tag -> title;
                $tags_html .= '<li><a href="/tag/'. $tag -> slug .'">' . $tag -> title . '</a></li>';
            }
        }
        $tags_html .= '</ul>';
        $page_keywords = implode(',', $keywords);
        break;
    case 'tag:tag':
        $tag_data = json_decode($data);
        $tag = $tag_data -> tag;
        $tag_title = $tag -> title;
        $page_title = $tag_title;
        $page_description = $page_title;
        $posts = $tag_data -> posts;
        $html = '<ul>';
        if ($posts) {
            foreach ($posts as $post) {
                $url = str_replace($api_url, '', $post -> url);
                $url = rtrim($url, '/');
                $html .= '<li>
              <h4><a href="' . $url . '">' . $post -> title_plain . '</a></h4>
              <div>' . strip_tags($post -> excerpt) . '</div>
            </li>';
            }
        }
        $html .= '</ul>';
        $tag_tag_html = $html;
        break;
    case 'category:category':
        $category_data = json_decode($data);
        $category = $category_data -> category;
        $category_title = $category -> title;
        $page_title = $category_title;
        $page_description = $page_title;
        $posts = $category_data -> posts;
        $html = '<ul>';
        if ($posts) {
            foreach($posts as $post) {
                $url = str_replace($api_url, '', $post -> url);
                $url = rtrim($url, '/');
                $html .= '<li>
              <h4><a href="' . $url . '">' . $post -> title_plain . '</a></h4>
              <div>' . strip_tags($post -> excerpt) . '</div>
            </li>';
            }
        }
        $html .= '</ul>';
        $category_category_html = $html;
        break;
    case 'blogpost':
        $data = json_decode($data) -> post;
        $page_content = $data -> content;
        $page_title = $data -> title_plain;
        $page_description = $page_title;
        $categories = $data -> categories;
        $keywords = array();
        if ($categories) {
            foreach ($categories as $category) {
                $keywords[] = $category -> title;
            }
        }
        $page_keywords = implode(',', $keywords);
        break;
}

ob_start("compress");
?>
<!doctype html>
<html lang="en-us">
<head>
  <meta charset="utf-8">
  <title><?= $page_title . $TITLE_SEPARATOR . $PAGE_TITLE; ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="manifest" href="/static/manifest.json">
  <link rel="shortcut icon" href="/static/icons/favicon.ico">
  <link rel="alternate" type="application/rss+xml" title="HTML5, CSS3 and JavaScript Â» Feed" href="http://api.martinivanov.net/feed/">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
  <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1">
  <meta name="fragment" content="!">
    <?php if($page_keywords): ?>
      <meta name="keywords" content="<?= $page_keywords; ?>" />
    <?php endif;?>
    <?php if($page_description): ?>
      <meta name="description" content="<?= $page_title; ?>" />
    <?php endif;?>
  <style>
    html
    {
      height: 100%;
    }
    body,
    html
    {
      background: url('/static/img/spinner.gif') no-repeat center;
    }
    [v-cloak]
    {
      opacity: 0;
      position: absolute;
      clip: rect(0, 0, 0, 0);
    }
  </style>

  <% for (key in htmlWebpackPlugin.files.css) { %>
  <link rel="stylesheet" href="<%= htmlWebpackPlugin.files.css[key] %>" />
  <% } %>

</head>
<body>
<div id="app" v-cloak>
  <h1><?= $page_title; ?></h1>
  <h2><a href="/"><?= $SITE_TITLE; ?></a></h2>
  <h3><?= $SITE_SUBTITLE; ?></h3>
  <div><?= $page_content; ?></div>
  <div><?= $categories_html; ?></div>
  <div><?= $tags_html; ?></div>
  <div><?= $tag_tag_html; ?></div>
  <div><?= $category_category_html; ?></div>
  <div><?= $recent_posts_html; ?></div>
  <h5>Categories and Tags</h5>
  <ul>
    <li><a href="/category">categories</a></li>
    <li><a href="/tag">tags</a></li>
  </ul>
  <h5>Links</h5>
  <ul>
    <li><a href="https://vuecidity.wemakesites.net/" target="_blank">Vuecidity - UI component library for VueJs</a></li>
    <li><a href="https://vueribbon.com/home" target="_blank">Ribbon component for VueJs</li>
    <li><a href="http://ribbonjs.com/home" target="_blank">Ribbon component for jQuery</li>
    <li><a href="https://wemakesites.net/web-dev" target="_blank">Martin Ivanov portfolio and personal website</li>
    <li><a href="http://experiments.wemakesites.net/" target="_blank">HTML5, CSS3 and JavaScript experiments and insight</a></li>
    <li><a href="http://em.wemakesites.net" target="_blank">RESTFul API for Encyclopaedia Metallum</a></li>
    <li><a href="http://elevenrack.net/home" target="_blank">Avid Eleven Rack presets and rigs</a></li>
  </ul>
  <p>&copy; 2006 - <?= Date('Y');?> <a href="/">Martin Ivanov</a></p>
</div>
<script src="//martinivanov.net/ads.js"></script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<% for (key in htmlWebpackPlugin.files.js) { %>
<script src="<%= htmlWebpackPlugin.files.js[key] %>"></script>
<% } %>

</body>
</html>