<?php
header("Access-Control-Allow-Origin: *"); // this needs to be removed
ob_start('ob_gzhandler'); // start g-zip handler
$apiEndpoint = 'http://api.martinivanov.net/api/'; // set your api endpoint
$resource = $_GET['resource'];
// initial bootstrap call
if (isset($_GET['bootstrap'])) {
    $recentPosts = json_decode(file_get_contents($apiEndpoint.'get_recent_posts/?count=' . $_GET['count']));
    $tags = json_decode(file_get_contents($apiEndpoint.'get_tag_index'));
    $categories = json_decode(file_get_contents($apiEndpoint.'get_category_index'));
    $infoBox = json_decode(file_get_contents($apiEndpoint.'get_page/?page_slug=' . $_GET['infoBarPageSlug']));
    echo json_encode(array(
        'recentPosts' => $recentPosts,
        'tags' => $tags -> tags,
        'categories' => $categories -> categories,
        'infoBox' => $infoBox -> page
    ));
} else {
    if ($resource) {
        $pathToResource = $apiEndpoint . $resource;
        switch ($resource) {
            case 'get_post':
                $fullPath = $pathToResource .'/?post_slug=' . $_GET['post_slug'];
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_page':
                $fullPath = $pathToResource .'/?page_slug=' . $_GET['page_slug'];
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_search_results':
                $fullPath = $pathToResource . '/?search=' . $_GET['search'] . '&count=' . $_GET['count'] . '&page=' . $_GET['page'];
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_category_index':
                $fullPath = $pathToResource;
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_category_posts':
                $fullPath = $pathToResource .'/?category_slug=' . $_GET['category_slug'] . '&count=200';
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_tag_posts':
                $fullPath = $pathToResource .'/?tag_slug=' . $_GET['tag_slug'] . '&count=200';
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_posts':
                $fullPath = $pathToResource .'/?count=' . $_GET['count'];
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
            case 'get_author_posts':
                $fullPath = $pathToResource .'/?slug=' . $_GET['slug'] . '&post_type=' . $_GET['post_type'] . '&count=' . $_GET['count'];;
                if (@file_get_contents($fullPath)) {
                    echo file_get_contents($fullPath);
                }
                break;
        }
    }
}